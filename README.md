# Building

## Prerequisites

    npm install -g concat-cli
    npm install -g typescript
    
## Building

    npm run build
    
The distributable file is found under `/dist/Socky.user.js` or `/dist/Socky.min.user.js` and can be pasted directly into tamper monkey.